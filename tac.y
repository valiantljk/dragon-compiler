/*
 Name: Jialin Liu
 Date: May 1st 2015
 Course: Compiler Construction, Dr. Richard Watson, CS5353
*/
%{
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<math.h>
#define START 100
typedef struct ListNode{
	int val;
	struct ListNode *next;
}ListNode;

typedef struct s
{
ListNode* true;
ListNode* false;
ListNode* next;
int quad;
char place[5];
}ETYPE;
int nextquad=START;
char code[25][50];
int yydebug=1;
char* Newtemp();
ListNode* Makelist(int nquad);
ListNode* Merge(ListNode* list1,ListNode* list2);
int Backpatch(ListNode* list,int nquad);
void Gen();
%}
 
%union
{
char id[10];
ETYPE eval;
}

%left "|"
%left "&"
%left "!"
%left "<" ">"
%left "+" "-"
%left "*" "/"
%left "(" ")"
%right "="

%token <id> LETTER INTEGER FLOAT
%token FOR WHILE DO IF THEN ELSE READ WRITE
%type <eval> PROGRAM BLOCK STATEMENTS ASSIGN COND LVAL E M N Q

%start PROGRAM
 
%%
PROGRAM: BLOCK {
int i;
Backpatch($1.next,nextquad);
for(i=START;i<nextquad;i++)
printf("\n%s",code[i-START]);
printf("\n%d\n",nextquad);
}
 
BLOCK: DO M BLOCK WHILE M COND {
Backpatch($6.true,$2.quad);
$$.next=$6.false;
}
| FOR ASSIGN M COND M ASSIGN Q BLOCK {
Backpatch($8.next,$5.quad);
Backpatch($6.next,$3.quad);
Backpatch($4.true,$7.quad);
$$.next=$4.false;
sprintf(code[nextquad-START],"%d\tgoto %d",nextquad,$5.quad);
Gen();
}
| WHILE M COND M BLOCK {
Backpatch($5.next,$2.quad);
Backpatch($3.true,$4.quad); 
$$.next=$3.false; 
sprintf(code[nextquad-START],"%d\tgoto %d",nextquad,$2.quad); 
Gen(); 
} 
| IF COND M BLOCK { 
Backpatch($2.true,$3.quad); 
$$.next=Merge($2.false,$4.next);
} 
| IF COND M BLOCK N ELSE M BLOCK { 
Backpatch($2.true,$3.quad);
Backpatch($2.false,$7.quad);
$$.next=Merge($4.next,$5.next);
$$.next=Merge($$.next,$8.next);
}
| '{' STATEMENTS '}' {
$$.next=$2.next;
}
| ASSIGN ';' {
ListNode * newnode;
newnode->val=0;
$$.next=newnode;
}
| READ ';'{
sprintf(code[nextquad-START],"%d\tread ",nextquad);
Gen(); 
}
| E { }

 
 
STATEMENTS: STATEMENTS M BLOCK {
Backpatch($1.next,$2.quad);
$$.next=$3.next;
} 
| BLOCK { 
$$.next=$1.next; 
} 
  
ASSIGN: LVAL '=' E { 
sprintf(code[nextquad-START],"%d\t%s liu %s",nextquad,$1.place,$3.place); 
Gen(); 
} 
| E { }  

LVAL: LETTER {strcpy($$.place,$1);}  

E: E '+' E { 
strcpy($$.place,Newtemp()); 
sprintf(code[nextquad-START],"%d\t%s = %s + %s",nextquad,$$.place,$1.place,$3.place); 
Gen(); 
} 
| E '-' E { 
strcpy($$.place,Newtemp()); 
sprintf(code[nextquad-START],"%d\t%s = %s - %s",nextquad,$$.place,$1.place,$3.place); 
Gen(); 
}
 
| E '*' E { 
strcpy($$.place,Newtemp()); 
sprintf(code[nextquad-START],"%d\t%s = %s * %s",nextquad,$$.place,$1.place,$3.place); 
Gen(); 
} 
| E '/' E { 
strcpy($$.place,Newtemp()); 
sprintf(code[nextquad-START],"%d\t%s = %s / %s",nextquad,$$.place,$1.place,$3.place); 
Gen(); 
} 
| '-' E %prec '*' { 
strcpy($$.place,Newtemp()); 
sprintf(code[nextquad-START],"%d\t%s = - %s",nextquad,$$.place,$2.place); 
Gen(); 
} 
| LETTER { 
strcpy($$.place,$1); 
} 
| INTEGER { 
strcpy($$.place,$1); 
} 
| FLOAT { 
strcpy($$.place,$1); 
} 
  
COND: COND '&' M COND { 
Backpatch($1.true,$3.quad); 
$$.true=$4.true; 
$$.false=Merge($1.false,$4.false); 
} 
| COND '|' M COND { 
Backpatch($1.false,$3.quad); 
$$.true=Merge($1.true,$4.true); 
$$.false=$4.false; 
} 
| '!' COND { 
$$.true=$2.false; 
$$.false=$2.true; 
} 
| '(' COND ')' { 
$$.true=$2.true; 
$$.false=$2.false; 
} 
| E '<' E { 
$$.true=Makelist(nextquad); 
$$.false=Makelist(nextquad+1); 
sprintf(code[nextquad-START],"%d\tif %s < %s goto ",nextquad,$1.place,$3.place); 
Gen(); 
sprintf(code[nextquad-START],"%d\tgoto ",nextquad); 
Gen(); 
} 
| E '>' E { 
$$.true=Makelist(nextquad); 
$$.false=Makelist(nextquad+1); 
sprintf(code[nextquad-START],"%d\tif %s > %s goto ",nextquad,$1.place,$3.place); 
Gen(); 
sprintf(code[nextquad-START],"%d\tgoto ",nextquad); 
Gen(); 
} 
| E { 
$$.true=Makelist(nextquad); 
$$.false=Makelist(nextquad+1); 
sprintf(code[nextquad-START],"%d\tif %s goto ",nextquad,$1.place); 
Gen(); 
sprintf(code[nextquad-START],"%d\tgoto ",nextquad); 
Gen(); 
}

M: { 
$$.quad=nextquad; 
} 
N: { 
$$.next=Makelist(nextquad); 
sprintf(code[nextquad-START],"%d\tgoto ",nextquad); 
Gen(); 
} 
Q: { 
$$.next=Makelist(nextquad); 
sprintf(code[nextquad-START],"%d\tgoto ",nextquad); 
Gen(); 
$$.quad=nextquad; 
}
 
%%
  
#include "lex.yy.c"

char* Newtemp() 
{ 
static int count=1; 
char* ch=(char*)malloc(sizeof(char)*5); 
sprintf(ch,"T%d",count++); 
return ch; 
} 
  
ListNode* Makelist(int nquad) 
{ 
ListNode* list=(ListNode *)malloc(sizeof(ListNode));
list->val=nquad;
ListNode* second=(ListNode *)malloc(sizeof(ListNode));;
second->val=0;
list->next=second;
return list; 
}
  
ListNode* Merge(ListNode* list1,ListNode* list2)
{
if(!list1&&!list2) return NULL;
if(!list2) return list1;
if(!list1) return list2;
ListNode* mergedlist=list1;
while(!list1->next){list1=list1->next;}
list1->next=list2;
free(list2);
return mergedlist;
}
 
int Backpatch(ListNode* list,int nquad)
{
char addr[10];
sprintf(addr,"%d",nquad);
ListNode* record=list;
while(record&&record->val!=0)
{
int index=record->val-START;
strcat(code[index],addr);
record=record->next;
}
return 1;
}
 
void Gen()
{
nextquad++;
}

int main ( int argc, char *argv[] ) {
yyin = fopen(argv[1], "r");
yyparse();
printf("Parse Sucessfully\n");
}
 
yyerror(char *errmesg) 
{ 

}  
